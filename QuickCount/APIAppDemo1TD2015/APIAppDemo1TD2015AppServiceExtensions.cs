using System;
using System.Net.Http;
using Microsoft.Azure.AppService;

namespace QuickCountClient
{
    public static class APIAppDemo1TD2015AppServiceExtensions
    {
        public static APIAppDemo1TD2015 CreateAPIAppDemo1TD2015(this IAppServiceClient client)
        {
            return new APIAppDemo1TD2015(client.CreateHandler());
        }

        public static APIAppDemo1TD2015 CreateAPIAppDemo1TD2015(this IAppServiceClient client, params DelegatingHandler[] handlers)
        {
            return new APIAppDemo1TD2015(client.CreateHandler(handlers));
        }

        public static APIAppDemo1TD2015 CreateAPIAppDemo1TD2015(this IAppServiceClient client, Uri uri, params DelegatingHandler[] handlers)
        {
            return new APIAppDemo1TD2015(uri, client.CreateHandler(handlers));
        }

        public static APIAppDemo1TD2015 CreateAPIAppDemo1TD2015(this IAppServiceClient client, HttpClientHandler rootHandler, params DelegatingHandler[] handlers)
        {
            return new APIAppDemo1TD2015(rootHandler, client.CreateHandler(handlers));
        }
    }
}
