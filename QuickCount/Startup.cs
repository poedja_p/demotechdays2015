﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(QuickCount.Startup))]
namespace QuickCount
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
