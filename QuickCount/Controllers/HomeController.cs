﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;


namespace QuickCount.Controllers
{
    public class HomeController : Controller
    {
        public async Task<ActionResult> Index()
        {


            QuickCountClient.APIAppDemo1TD2015 app = new QuickCountClient.APIAppDemo1TD2015();
            QuickCountClient.QuickCountOperations client = new QuickCountClient.QuickCountOperations(app);
            var result = await client.GetWithOperationResponseAsync();
            
            
            return View(result.Body);
        }

    }
}